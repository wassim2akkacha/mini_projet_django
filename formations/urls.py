from django.contrib import admin
from django.urls import path,include
from . import views

urlpatterns = [
    path('formations/',views.formations_list,name='formations'),
    path('ajouter_formation/',views.ajouter_formation,name='ajouter_formation'),
    path('home/',views.home,name='home'),
    path('modifier_formation/<str:pk>/',views.modifier_formation,name='modifier_formation'),
    path('supprimer_formation/<str:pk>/', views.supprimer_formation, name="supprimer_formation"),
    path('formation_detail/<str:pk>/', views.formation_detail, name="formation_detail"),
]
