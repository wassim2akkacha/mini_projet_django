from django.db import models

# Create your models here.
from django.db import models  
class Formation(models.Model):  
    titre = models.CharField(max_length=20)  
    logo=models.ImageField(null=False)
    body = models.TextField()
    CHOICES = (
        ('active', 'active'),
        ('non active', 'non active'),
    )
    etat = models.CharField(max_length=300, choices = CHOICES)
    CHOICES2 = (
        ('Web', 'Web'),
        ('Cloud', 'Cloud'),
        ('Mobile', 'Mobile'),
        ('Devops', 'Devops'),
        ('Artificial Intelligence','Artificial Intelligence')
    )
    categorie = models.CharField(max_length=300, choices = CHOICES2,null=True)
    date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.titre


