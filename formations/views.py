from django.shortcuts import render,redirect
from .forms import FormationForm
from .models import Formation
from .filters import FormationFilter
from django.contrib.auth.decorators import login_required
from .decorators import admin_only
# Create your views here.
def home(request):
    #return HttpResponse('homepage')
    return render(request,"home.html")

@login_required(login_url='login')
def formations_list(request):
    formations = Formation.objects.all()

    myFilter = FormationFilter(request.GET,queryset=formations)
    formations = myFilter.qs
    return render(request, "formations/formations_list.html",{"formations":formations, "myfilter" : myFilter})

@login_required(login_url='login')
@admin_only
def ajouter_formation(request):
    form= FormationForm()
    if request.method == 'POST':
        form=FormationForm(request.POST,request.FILES)
        if form.is_valid():
            form.save()
            return redirect("../home")
    return render(request,"formations/formation_form.html",{'form' : form})

@login_required(login_url='login')
@admin_only
def modifier_formation(request,pk):
    formation = Formation.objects.get(id=pk)
    form= FormationForm(instance = formation)
   
    if request.method == 'POST':
        form=FormationForm(request.POST,instance = formation)
        if form.is_valid():
            form.save()
            return redirect("formations")

    return render(request,"formations/formation_form.html",{'form' : form})

@login_required(login_url='login')
@admin_only
def supprimer_formation(request, pk):
	formation = Formation.objects.get(id=pk)
	if request.method == "POST":
		formation.delete()
		return redirect('formations')
	return render(request, 'formations/supprimer_formation.html', {'formation':formation})

@login_required(login_url='login')
def formation_detail(request,pk):
    formation = Formation.objects.get(id=pk)
    return render(request, 'formations/formation_detail.html', {'formation':formation})







    
