from django.shortcuts import render, redirect 
from django.http import HttpResponse
from django.forms import inlineformset_factory
from django.contrib.auth.forms import UserCreationForm

from django.contrib.auth import authenticate, login, logout

from django.contrib import messages
from .forms import loginForm
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import Group

# Create your views here.
def loginPage(request):
    if request.user.is_authenticated:
        return redirect('home')
    else:
        form=loginForm()
        if request.method == 'POST':
            password = request.POST.get('password')
            username = request.POST.get('username')
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request,user)
                return redirect('home')
            else :
                return redirect('login')

    return render(request,'authentication/login.html',{'form':form})

def register(request):
    if request.user.is_authenticated:
        return redirect('home')
    else:
        form = UserCreationForm()
        if request.method == 'POST':
            form = UserCreationForm(request.POST)
            if form.is_valid():
                user = form.save()
                group = Group.objects.get(name = 'simpleUser')
                user.groups.add(group)
                return redirect('login')
    return render(request,'authentication/register.html',{'form' : form})

def logoutUser(request):
	logout(request)
	return redirect('login')
